const _createItemActionButton = (todoItemId, clickHandler) => {
  const button = document.createElement("button");
  // register the click handler callback
  button.addEventListener("click", clickHandler);
  // sets a custom attribute for easy access to the id in the click handler event object
  button.setAttribute("todoId", todoItemId);
  // add the button classes for CSS styling
  button.classList.add("mui-btn", "mui-btn--small", "mui-btn--fab");

  return button;
};

const _createCompleteButtonComponent = (todoItemId, clickHandler) => {
  const baseActionButton = _createItemActionButton(todoItemId, clickHandler);
  // add the check-mark button text
  baseActionButton.innerText = "✓";
  // add the complete button CSS style class
  baseActionButton.classList.add("mui-btn--primary");

  return baseActionButton;
};

const _createDeleteButtonComponent = (todoItemId, clickHandler) => {
  const baseActionButton = _createItemActionButton(todoItemId, clickHandler);
  // add the check-mark button text
  baseActionButton.innerText = "✘";
  // add the complete button CSS style class
  baseActionButton.classList.add("mui-btn--danger");

  return baseActionButton;
};

const _createItemTextSpanComponent = (todoItemText) => {
  const span = document.createElement("span");
  span.innerText = todoItemText;
  span.classList.add("item-text");

  return span;
};

const _createTodoListItem = (todoItem) => {
  // create the DOM element
  const li = document.createElement("li");
  // set its CSS classes
  li.classList.add("mui-panel", "mui--text-dark", "mui--text-display1");

  // create the <span> element that holds the Todo Item text
  const itemTextSpan = _createItemTextSpanComponent(todoItem.text);

  // append the button and item text elements
  li.append(itemTextSpan);

  // return the fully formed incomplete Todo Item <li> HTML element representation
  return li;
};

/**
 * Creates an incomplete Todo Item list (<li>) element with styling and child elements already appended
 *
 * Usage:
 * - when you submit a POST request for creating a new item you will receive that Todo Item in the response
 * - Use this function to create an HTML <li> representation of that item by providing it with the Todo Item object and the click handler for when its "complete item check-mark" button is clicked
 *
 * Complete item button handler:
 * - should submit the PATCH request for updating the item's completion status
 * - you can get the item's ID from the click event object with:
 *
 * ```js
 * event.target.getAttribute("todoId")
 * ```
 *
 * @param {{ id: number, text: string, completed: boolean }} todoItem  a newly created Todo Item
 * @param {function} completeItemButtonHandler a handler callback for a click event on the complete (check-mark) button
 * @returns {HTMLElement} the new <li> item for visually presenting an incomplete Todo Item as an HTML element
 * 
 * @example
 * ```js
  // in practice this should be the item returned from the POST request
  const todoItemSample = {
    id: 1,
    text: "sample text",
    completed: false,
  };

  const markItemComplete = (event) => {
    const todoId = event.target.getAttribute("todoId"), // 1 (the sample item's ID)
    console.log({
      event,
      todoId
    });

    // use the id to send the PATCH request
  };

  const incompleteListItem = createIncompleteListItemComponent(
    todoItemSample,
    markItemComplete
  );

  const incompleteItemsList = document.querySelector("#incomplete-list");
  incompleteItemsList.appendChild(incompleteListItem);
  ```
 */
const createIncompleteListItemComponent = (
  todoItem,
  completeItemButtonHandler
) => {
  // create the DOM element
  const baseListItem = _createTodoListItem(todoItem);

  // create the complete button with the handler for its click event
  const completeButton = _createCompleteButtonComponent(
    todoItem.id,
    completeItemButtonHandler
  );

  // prepend (insert before the item text span element) the button so it appears to the left of the text
  baseListItem.prepend(completeButton);

  // return the fully formed incomplete Todo Item <li> HTML element representation
  return baseListItem;
};

/**
 * Creates a completed Todo Item list (<li>) element with styling and child elements already appended
 *
 * Usage:
 * - when an item is marked as complete (after sending the PATCH request) you should take the returned item (with { completed: true }) and pass it to this function
 * - Use this function to create an HTML <li> representation of that completed item by providing it with the updated Todo Item object and the click handler for when its "delete item x-mark" button is clicked
 *
 * Delete item button handler:
 * - should submit the DELETE request for deleting the Todo Item
 * - you can get the item's ID from the click event object with:
 *
 * ```js
 * event.target.getAttribute("todoId")
 * ```
 *
 * @param {{ id: number, text: string, completed: boolean }} todoItem  a newly created Todo Item
 * @param {function} deleteItemButtonHandler a handler callback for a click event on the delete (x-mark) button
 * @returns {HTMLElement} the new <li> item for visually presenting a completed Todo Item as an HTML element
 * 
 * @example
 * ```js
  // in practice this should be the item returned from the PATCH request
  const todoItemSample = {
    id: 1,
    text: "sample text",
    completed: false,
  };

  const deleteItem = (event) => {
    const todoId = event.target.getAttribute("todoId"), // 1 (the sample item's ID)
    console.log({
      event,
      todoId
    });

    // use the id to send the DELETE request
  };

  const completedListItem = createCompletedListItemComponent(
    todoItemSample,
    deleteItem
  );

  const completedItemsList = document.querySelector("#completed-list");
  incompleteItemsList.appendChild(completedListItem);
  ```
 */
const createCompletedListItemComponent = (
  todoItem,
  deleteItemButtonHandler
) => {
  // create the DOM element
  const baseListItem = _createTodoListItem(todoItem);

  // create the complete button with the handler for its click event
  const deleteButton = _createDeleteButtonComponent(
    todoItem.id,
    deleteItemButtonHandler
  );

  // prepend (insert before the item text span element) the button so it appears to the left of the text
  baseListItem.prepend(deleteButton);

  // return the fully formed complete Todo Item <li> HTML element representation
  return baseListItem;
};

// uncomment to view an example usage
// document.addEventListener("DOMContentLoaded", () => {
//   const todoItemSample = {
//     id: 1,
//     text: "sample text",
//     completed: false,
//   };

//   const markItemComplete = (event) => {
//     console.log("complete item button clicked", {
//       event,
//       id: event.target.getAttribute("todoId"),
//     });
//   };

//   const incompleteListItem = createIncompleteListItemComponent(
//     todoItemSample,
//     markItemComplete
//   );

//   const deleteItem = (event) => {
//     console.log("delete item button clicked", {
//       event,
//       id: event.target.getAttribute("todoId"),
//     });
//   };

//   const completedListItem = createCompletedListItemComponent(
//     todoItemSample,
//     deleteItem
//   );

//   const incompleteItemsList = document.querySelector("#incomplete-list");
//   incompleteItemsList.append(incompleteListItem);

//   const completedItemsList = document.querySelector("#completed-list");
//   completedItemsList.append(completedListItem);
// });
