# Todo Client Project

Start working on this project:

1. fork the repo
2. copy the SSH or HTTPS URL of the repo to your clipboard
3. clone the repo locally

```sh
$ git clone <paste repo URL> ~/gis-devops/todo-app
```

## Client (Front End) Usage

> Running the client

```sh
# run from the root repo directory (todo-app/)
$ python3 -m http.server -d client/src 3000
```

Open your browser to `http://localhost:3000`

## Functional Requirements

The goal of this project is to create JavaScript script(s) that will support the following behaviors:

> on page load

- request the list of todo items from the API
- render the items in the appropriate list (see starter code section below) according to each item's `completed` property (see API section below)

> when the new item form is submitted (by submit button click or the enter key)

- submit a `POST` request to the API to create a new item (see API section below) using the text value

> when an item in the uncompleted items list has its check-mark button pressed

- submit a `PATCH` request to the API to update its `completed` property value (see API section below)

> when an item in the completed items list has its x-mark button pressed

- submit a `DELETE` request to the API to delete the item (see API section below)

## Starter Code

```sh
todo-app/ <-- repo dir
  client/ <-- front end
    src/ <-- front end code
      index.html <-- base HTML and styling
      style.css  <-- overrides of MUI style library
```

> Element Ids

You can use this list to select the elements you will be working with.

```js
const elementIds = [
  "todo-text", // text input for new item
  "new-todo-form", // form with todo-text input
  "completed-list", // to display items with completed: true
  "incomplete-list", // to display items with completed: false
  "create-item-button", // submit button for new todo item form
];
```

# API Usage

> Running the API locally

You can host the API locally using a Docker container with the following command:

```sh
$ docker run -d -p 8008:8008 --name todo-api launchcodedevops/todo-api:node
```

You can then consume the API at the `http://localhost:8008` origin.

## API Data

The Todo API uses JSON for all data send and received by it. In HTTP terms this means the `Content-Type` header of all requests must be and responses will be `application/json`.

- client `POST` requests that include a request body **must have** the `Content-Type` header set to `application/json`
- all API responses that include a body (`GET`, `POST`, `PATCH`) will have this header set as well

> Status Codes: **be careful with responses that do not include a body!**

All requests to entity-specific endpoints (`/todos/:id`) will return a `404` (not found) status code if the item does not exist in the collection. This means **the response will not have a JSON body** which _could cause issues with JSON parsing._

> Todo Item Shape

All todo items will include a unique `id` property, `text` for the todo item itself and a `completed` boolean property indicating its status:

```json
Todo Item {
  id: number
  text: string
  completed: boolean
}
```

# API Endpoints

All requests should be sent to the API origin of:

- the locally hosted API (Docker container): `http://localhost:8008`
- the publicly hosted API: ask your instructor for the public IP address

Note that the public API will include all todo items for anyone else who is interacting with it! You should start with your own local API (to ensure consistent behavior during development) but you can switch to the public one if you would like.

Endpoints include the HTTP method and path (`METHOD /path`) that should be appended to the API origin

> Example

- endpoint: `GET /todos`
- request: a `GET` request to `http://localhost:8008/todos`

## Retrieve all Todo Items

> Endpoint: `GET /todos`

Returns a list of all todo items.

> example request with `curl`

```sh
$ curl http://localhost:8008/todos
```

> example response

```json
[
  {
    "id": 1,
    "text": "sample text",
    "completed": false
  },

  {
    "id": 2,
    "text": "sample text",
    "completed": true
  }
]
```

## Create a new Todo Item

> Endpoint: `POST /todos`

Creates and returns a new todo item. The request **must include** a `Content-Type` header with a value of `application/json`

> example request with `curl`

```sh
$ curl -X POST http://localhost:8008/todos -d '{"text": "complete the todo app client"}' -H 'content-type:application/json'
```

> example response

```json
{
  "id": 1,
  "text": "text from form input",
  "completed": false
}
```

## Mark a Todo Item as completed

> Endpoint: `PATCH /todos/:id`

Marks a todo item as completed. The item is identified by the `:id` path variable.

> example request

To update the todo item with an `id` property of `6` you would send the following `PATCH` request using `curl`:

```sh
$ curl -X PATCH http://localhost:8008/todos/6
```

> example response body

```json
{
  "id": 6,
  "text": "complete the todo app client",
  "completed": true
}
```

## Delete a Todo Item

> Endpoint: `DELETE /todos/:id`

Deletes an item. The item is identified by the `:id` path variable.

> example request

To delete the todo item with an `id` property of `6` you would send the following `DELETE` request using `curl`:

```sh
$ curl -X DELETE http://localhost:8008/todos/6
```

> example response

`DELETE` requests traditionally do not include a response body. However, the status code `204` (no content) will indicate its success as part of the `2XX` status codes group.

# References

The following is a list of JavaScript browser APIs and other related reference links that you should look into to complete the functional requirements:

## AJAX Requests

- [fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
- [Response object](https://developer.mozilla.org/en-US/docs/Web/API/Response)
- [response.json()](https://developer.mozilla.org/en-US/docs/Web/API/Body/json)
- [JSON.stringify()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify)
- [JSON.parse()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse)

## Promises

- [using Promises](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises)
- [Promise object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)
- [promise.then()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/then)
- [promise.catch()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/catch)
- [async functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function)

## Arrays

- [array.filter()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)
- [array.map()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)

## Strings

- [template strings](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)

## DOM

- [document.querySelector()](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector)
- [document.querySelectorAll()](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll)
- [eventTarget.addEventListener()](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener)
- [DOM events reference](https://developer.mozilla.org/en-US/docs/Web/Events)
- [HTML form elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form)
- [HTML form input elements](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input)
- [overriding default HTML form submission behavior](https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement/submit_event#Examples)


## Bonus

Only after completing all of the requirements should you attempt the Bonus objectives.

- Make the Client your own. Edit the HTML, CSS, and JS as you see fit to change how the client looks, feels, and behaves
- Add a dropdown that allows the user to change which API they consume
- Containerize the client using Docker
- Deploy to S3
- Build the API in Node/Express yourself

Reach out to your instructor if you are unsure on how to achieve any of the bonus objectives.
